<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
  <title>Team Page</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <style type="text/css">
    body {
      margin-top: 60px;
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Teams</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#/">Home</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>

<div class="container">


  <div class="panel panel-default">
    <div class="panel-heading"> Add a team</div>
    <div class="panel-body">

      <c:url var="addAction" value="/team/add" ></c:url>

      <form:form action="${addAction}" commandName="team">
        <table class="table">
          <c:if test="${!empty team.name}">
            <tr>
              <td>
                <form:label path="id">
                  <spring:message text="ID"/>
                </form:label>
              </td>
              <td>
                <form:input path="id" readonly="true" size="8"  disabled="true" />
                <form:hidden path="id" />
              </td>
            </tr>
          </c:if>
          <tr>
            <td>
              <form:label path="name">
                <spring:message text="Name"/>
              </form:label>
            </td>
            <td>
              <form:input path="name" />
            </td>
          </tr>
          <tr>
            <td>
              <form:label path="rating">
                <spring:message text="Rating"/>
              </form:label>
            </td>
            <td>
              <form:input path="rating" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <c:if test="${!empty team.name}">
                <input type="submit" class="btn btn-warnnig"
                       value="<spring:message text="Edit Team"/>" />
              </c:if>
              <c:if test="${empty team.name}">
                <input type="submit" class="btn btn-primary"
                       value="<spring:message text="Add Team"/>" />
              </c:if>
            </td>
          </tr>
        </table>
      </form:form>



    </div>
  </div>

  <br>


  <div class="panel panel-default">
    <div class="panel-heading">Teams List</div>
    <div class="panel-body">


      <c:if test="${!empty listTeams}">
        <table class="table table-bordered tg">
          <tr>
            <th>Team ID</th>
            <th>Team Name</th>
            <th>Team rating</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
          <c:forEach items="${listTeams}" var="team">
            <tr>
              <td>${team.id}</td>
              <td>${team.name}</td>
              <td>${team.rating}</td>
              <td><a href="<c:url value='/edit/${team.id}' />" class="btn btn-link">Edit</a></td>
              <td><a href="<c:url value='/remove/${team.id}' />"  class="btn btn-link">Delete</a></td>
            </tr>
          </c:forEach>
        </table>
      </c:if>

    </div>
  </div>






</div>
</body>
</html>
